package cyoa

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// A function t

// parse the JSON file
// start adventure
// shows current state, returns possible options
// a way of allowing the user to choose an option

// Story is a map of chapters keyed by the chapter name.
type Story map[string]chapter

type chapter struct {
	Title   string   `json:"title"`
	Story   []string `json:"story"`
	Options []struct {
		Text string `json:"text"`
		Arc  string `json:"arc"`
	} `json:"options"`
}

func readStoryFile() []byte {
	const filePath = "./gopher.json"
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(nil)
	}
	return data
}

func ParseStory() Story {
	data := readStoryFile()
	var story Story
	err := json.Unmarshal(data, &story)
	if err != nil {
		panic(err)
	}
	return story
}

func Start() {
	story := ParseStory()
	// "intro" is always expected to be a chapter and the first chapter
	chapterName := "intro"

	for {
		chapter := story[chapterName]
		options := chapter.Options
		fmt.Printf("Chapter name: %s\n", chapterName)
		fmt.Printf("Title: %s\n", chapter.Title)
		for _, storyLine := range chapter.Story {
			fmt.Printf("%v\n", storyLine)
		}
		numOptions := len(options)
		if numOptions == 0 {
			fmt.Println("This is the end of your adventure!")
			return
		}
		fmt.Printf("\n\nYou have %d option(s):\n", numOptions)
		for i, option := range options {
			fmt.Printf("%d: %v\n", i+1, option.Text)
		}
		fmt.Println("Which option do you choose?")
		var optionChoice int
		for {
			_, err := fmt.Scanln(&optionChoice)
			if err != nil {
				fmt.Printf("Couldn't parse input, try again [error: %v]\n", err)
				continue
			}
			if optionChoice < 1 || optionChoice > numOptions {
				fmt.Println("You've provided an invalid option, try again!")
				continue
			}
			break
		}
		fmt.Printf("======================\n\n")
		optionIndex := optionChoice - 1 // choices start from 1
		chapterName = options[optionIndex].Arc

	}
}
