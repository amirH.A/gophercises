package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"strings"
	"time"
)

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func getAnswer() string {
	var userAnswer string
	fmt.Scanln(&userAnswer)
	return strings.TrimSpace(userAnswer)
}

func askQuestion(question, answer string) bool {
	fmt.Println(question)
	userAnswer := getAnswer()
	return strings.ToLower(userAnswer) == strings.ToLower(answer)
}

func askAllQuestions(records *[][]string, score *int, done chan<- bool) {
	for _, record := range *records {
		if askQuestion(record[0], record[1]) {
			*score++
		}
	}
	done <- true
}

func main() {
	filePath := flag.String("in", "./problems.csv", "File path to the CSV file")
	timeLimit := flag.Duration("time-limit", 30*time.Second, "Time limit for the quiz")
	shuffle := flag.Bool("shuffle", false, "Shuffle the questions before asking them")
	flag.Parse()

	score := 0

	file, err := ioutil.ReadFile(*filePath)
	check(err)
	r := csv.NewReader(strings.NewReader(string(file)))

	records, err := r.ReadAll()
	check(err)
	if *shuffle {
		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(records), func(i, j int) { records[i], records[j] = records[j], records[i] })
	}
	done := make(chan bool)

	fmt.Println("Press enter to start the timer")
	var input string
	fmt.Scanln(&input)
	ctx, cancel := context.WithTimeout(context.Background(), *timeLimit)
	defer cancel()
	go askAllQuestions(&records, &score, done)
	select {
	case <- done:
		fmt.Println("Nicely done, within time!")
	case <- ctx.Done():
		fmt.Println("Time's up!")
	}

	fmt.Printf("%d out of %d correct answers\n", score, len(records))
}
