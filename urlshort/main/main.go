package main

import (
	"flag"
	"fmt"
	"github.com/syndtr/goleveldb/leveldb"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/amir97/gophercises/urlshort"
)

func main() {
	yamlFilePath := flag.String("yaml-path", "", "[Optional] Path to YAML file to load for the YAML handler")
	flag.Parse()
	mux := defaultMux()

	// Build the MapHandler using the mux as the fallback
	pathsToUrls := map[string]string{
		"/urlshort-godoc": "https://godoc.org/github.com/gophercises/urlshort",
		"/yaml-godoc":     "https://godoc.org/gopkg.in/yaml.v2",
	}
	mapHandler := urlshort.MapHandler(pathsToUrls, mux)

	// Build the YAMLHandler using the mapHandler as the
	// fallback
	yaml := `
- path: /urlshort
  url: https://github.com/gophercises/urlshort
- path: /urlshort-final
  url: https://github.com/gophercises/urlshort/tree/solution
`
	if *yamlFilePath != "" {
		yamlBytes, err := ioutil.ReadFile(*yamlFilePath)
		if err != nil {
			panic(err)
		}
		yaml = string(yamlBytes)
	}
	yamlHandler, err := urlshort.YAMLHandler([]byte(yaml), mapHandler)
	if err != nil {
		panic(err)
	}

	db, err := leveldb.OpenFile("./leveldbFile", nil)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	leveldbHandler, err := urlshort.LevelDBHandler(db, yamlHandler)
	if err != nil {
		panic(err)
	}
	fmt.Println("Starting the server on :8080")
	http.ListenAndServe(":8080", leveldbHandler)
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintln(w, "Hello, world!")
	if err != nil {
		log.Printf("unexpected error at hello handler: %v", err)
	}
}
