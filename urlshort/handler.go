package urlshort

import (
	"github.com/syndtr/goleveldb/leveldb"
	"gopkg.in/yaml.v2"
	"net/http"
)

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		matchedURL := pathsToUrls[request.URL.Path]
		if matchedURL != "" {
			http.Redirect(writer, request, matchedURL, http.StatusFound)
		} else {
			fallback.ServeHTTP(writer, request)
		}
	}
}

type pathToURL struct {
	Path string `yaml:"path"`
	URL  string `yaml:"url"`
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
func YAMLHandler(yml []byte, fallback http.Handler) (http.HandlerFunc, error) {
	var data []pathToURL
	err := yaml.Unmarshal(yml, &data)
	if err != nil {
		return nil, err
	}
	pathMap := buildMap(&data)
	return MapHandler(pathMap, fallback), nil
}

func buildMap(pathToURLs *[]pathToURL) map[string]string {
	result := make(map[string]string, len(*pathToURLs))
	for _, pathToURL := range *pathToURLs {
		result[pathToURL.Path] = pathToURL.URL
	}
	return result
}

func LevelDBHandler(db *leveldb.DB, fallback http.Handler) (http.HandlerFunc, error) {
	return func(writer http.ResponseWriter, request *http.Request) {
		matchedURLBytes, err := db.Get([]byte(request.URL.Path), nil)
		if err != nil {
			if err != leveldb.ErrNotFound {
				panic(err)
			} else {
				fallback.ServeHTTP(writer, request)
			}
		} else {
			if string(matchedURLBytes) != "" {
				http.Redirect(writer, request, string(matchedURLBytes), http.StatusFound)
			} else {
				fallback.ServeHTTP(writer, request)
			}
		}
	}, nil
}
